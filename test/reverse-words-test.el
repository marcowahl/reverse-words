;;; reverse-words-tests.el --- reverse words in region -*- lexical-binding: t -*-

;; [[id:bbb0bd6c-a504-4a25-a3d4-66d740bb2da7][Test:3]]
(require 'reverse-words)

(ert-deftest 4ab7f57926707acbc36561c78c3466cfe47e07ef ()
  (should
   (equal
    ""
    (with-temp-buffer
      (set-mark (point-min))
      (insert "")
      (call-interactively #'reverse-words)
      (buffer-substring (point-min) (point-max))))))

(ert-deftest 7cf9a9d7756ea7291634f5f337e6186964ac10f4 ()
  (should
   (equal
    " "
    (with-temp-buffer
      (set-mark (point-min))
      (insert " ")
      (call-interactively #'reverse-words)
      (buffer-substring (point-min) (point-max))))))

(ert-deftest 4d8a6cbd2256eefa41266fb04ce61bdb960a6fdb ()
  (should
   (equal
    "a"
    (with-temp-buffer
      (set-mark (point-min))
      (insert "a")
      (call-interactively #'reverse-words)
      (buffer-substring (point-min) (point-max))))))

(ert-deftest 24b4b378bec7118697be93c9edc821b32eac4e4a ()
  (should
   (equal
    "   a"
    (with-temp-buffer
      (set-mark (point-min))
      (insert "a   ")
      (call-interactively #'reverse-words)
      (buffer-substring (point-min) (point-max))))))

(ert-deftest 83e542ea5fb085b0d6def0017bbe7b54df32f12f ()
  (should
   (equal
    "a   "
    (with-temp-buffer
      (set-mark (point-min))
      (insert "   a")
      (call-interactively #'reverse-words)
      (buffer-substring (point-min) (point-max))))))

(ert-deftest dec6b59445c9509e51af12ffac8582ce342fe60d ()
  (should
   (equal
    "ccc bbb aaa"
    (with-temp-buffer
      (set-mark (point-min))
      (insert "aaa bbb ccc")
      (call-interactively #'reverse-words)
      (buffer-substring (point-min) (point-max))))))

(ert-deftest 101c85908e9a3290eb46323bda18deb3eafd0f2b ()
  (should (equal
           "bbb aaa"
           (with-temp-buffer
             (insert "aaa bbb")
             (set-mark (point))
             (goto-char (point-min))
             (call-interactively #'reverse-words)
             (buffer-substring (point-min) (point-max))))))

(ert-deftest c7304bde71b58a8a73c799754dbda5f0970b74fd ()
  (should (equal
           "ccc  bbb, aaa"
           (with-temp-buffer
             (set-mark (point-min))
             (insert "aaa, bbb  ccc")
             (call-interactively #'reverse-words)
             (buffer-substring (point-min) (point-max))))))

(ert-deftest 91d29df481e3342eba5333f24b674a79da6339e1 ()
  (should (equal
           "x

ccc  bbb, aaa"
           (with-temp-buffer
             (insert "x

aaa, bbb  ccc")
             (set-mark 4)
             (cl-assert (= (point-max) (point)))
             (call-interactively #'reverse-words)
             (buffer-substring (point-min) (point-max))))))

(ert-deftest 00a6e478eff8fa59fb0ca641644f900994a5d786 ()
  (should (equal
           "

ccc  bbb, aaa"
           (with-temp-buffer
             (insert "

aaa, bbb  ccc")
             (set-mark 3)
             (cl-assert (= (point-max) (point)))
             (call-interactively #'reverse-words)
             (buffer-substring (point-min) (point-max))))))

(ert-deftest 9fcfe050398f28d9b7d3466b5d61ba3d8e3e305d ()
  "leading newline gets trailing newline"
  (should (equal
           "
ccc  bbb, aaa
"
           (with-temp-buffer
             (insert "

aaa, bbb  ccc")
             (set-mark 2)
             (cl-assert (= (point-max) (point)))
             (call-interactively #'reverse-words)
             (buffer-substring (point-min) (point-max))))))


(provide 'reverse-words-test)
;; Test:3 ends here


;;; reverse-words-tests.el ends here
