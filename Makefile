LITERATE_SOURCE=reverse-words.org

ELISP_FILES=reverse-words.el test/reverse-words-test.el

all: compile test package

compile: $(ELISP_FILES)

$(ELISP_FILES): $(LITERATE_SOURCE)
	emacs --batch --eval '(progn (find-file "reverse-words.org") (org-babel-tangle))'

test: compile
	cask exec ert-runner -L .

package: compile
	cask package

clean:
	rm $(ELISP_FILES)
	-rm -r dist

.PHONY: test clean
