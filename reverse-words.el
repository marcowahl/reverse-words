;;; reverse-words.el --- reverse words in region -*- lexical-binding: t -*-

;; THIS FILE HAS BEEN GENERATED.

;; Code
;; :PROPERTIES:
;; :ID:       1c228439-a35d-469b-9def-2f8eac6e5f1c
;; :END:


;; [[id:1c228439-a35d-469b-9def-2f8eac6e5f1c][Code:1]]
;;; Version: 0.0.0


;;; Commentary:

;; M-x reverse-words

;; to reverse the words in the region.


;;; Code:

;;;###autoload
(defun reverse-words (beg end)
  "Reverse the order of the words in the region.
Keep the separation strings between the words.
Argument BEG beginning of region.
Argument END end of region."
  (interactive "r")
  (let ((moving-end end)
        strings)
    (goto-char moving-end)
    (while (< beg moving-end)
      (backward-word)
      (forward-word)
      (let ((moving-end-at-eow
             (if (<= beg (point) moving-end)
                 (point)
               beg)))
        (setf strings (cons (buffer-substring moving-end-at-eow moving-end) strings))
        (goto-char moving-end-at-eow)
        (backward-word)
        (let ((moving-end-at-bow (max (point) beg)))
          (setf strings
                (cons
                 (buffer-substring moving-end-at-bow moving-end-at-eow)
                 strings))
          (setf moving-end moving-end-at-bow)
          (goto-char moving-end))))
    (delete-region beg end)
    (insert (mapconcat #'identity (reverse strings) ""))))


(provide 'reverse-words)
;; Code:1 ends here


;;; reverse-words.el ends here
